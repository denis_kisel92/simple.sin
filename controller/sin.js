function Sinus(amplitude, startX, startY, lengthX)
{
    this.pulseTimeout;
    this.amplitude = amplitude;
    this.frequency = 1;

    this.startX = startX;
    this.startY = startY;

    this.lengthX = lengthX;

    this.pulseSpeed = 1.5;

    this.pulseClassName = 'sinPulse';
    this.pixelClassName = 'sinPixel';

    this.trajectoryOn = function() {

        for (var iteration = 0; iteration <= this.lengthX; iteration ++) {
            var positionX = this.startX + iteration;
            var positionY = this.startY + (Math.sin(-iteration * Math.PI/180 * this.frequency) * this.amplitude);

            var sinPixel = document.createElement('div');
            sinPixel.className = this.pixelClassName;

            sinPixel.style.left = positionX + 'px';
            sinPixel.style.top = positionY + 'px';

            document.body.appendChild(sinPixel);
        }
    };

    this.trajectoryOff = function() {

        var pixels = document.getElementsByClassName('sinPixel');

        for (var key = pixels.length; key > 0; key--) {
            document.body.removeChild(pixels[key - 1]);
        }

    };

    this.pulse = function(degree) {

        clearTimeout(this.pulseTimeout);

        if (degree == '' || degree >= this.lengthX) {
            degree = 0;
        }

        if (document.getElementsByClassName(this.pulseClassName)[0] == null) {
            var pulse = document.createElement('div');
            pulse.className = this.pulseClassName;
            document.body.appendChild(pulse);
        } else {
            var pulse = document.getElementsByClassName(this.pulseClassName)[0];
        }

        var sinDegree = (Math.sin(-degree * Math.PI/180 * this.frequency) * this.amplitude);

        pulse.style.left = this.startX + degree + 'px';
        pulse.style.top = this.startY + sinDegree + 'px';

        degree += Math.abs(Number(this.pulseSpeed));

        var selfObject = this;
        this.pulseTimeout = setTimeout(function() {selfObject.pulse(degree)}, 1);
    };

    this.pulseRemove = function() {
        clearTimeout(this.pulseTimeout);
        var pulse = document.getElementsByClassName(this.pulseClassName);

        for (var key = pulse.length; key > 0; key--) {
            document.body.removeChild(pulse[key - 1]);
        }
    };

}