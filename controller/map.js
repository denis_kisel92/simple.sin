function Map(mapId)
{
    this.mapId = mapId;
    this.mapContent = document.getElementById(this.mapId);
    this.startPointX = '';
    this.startPointY = '';

    this.markStep = 50;

    this.lineX = '';
    this.lineY = '';

    this.lengthX = '';
    this.lengthY = '';

    this.create = function() {
        console.log('create');

        this.createLines();

        this.setStartPoints();

        this.markX();
        this.markY();

    };

    this.createLines = function() {
        var lineX = document.createElement('div');
        lineX.id = 'lineX';

        var lineY = document.createElement('div');
        lineY.id = 'lineY';

        this.mapContent.appendChild(lineX);
        this.mapContent.appendChild(lineY);

        this.lineX = lineX;
        this.lineY = lineY;

        this.lengthX = lineX.offsetWidth;
        this.lengthY = lineY.offsetHeight;
    };

    this.setStartPoints = function() {
        var lineXAbsoluteCoordination = this.lineX.getBoundingClientRect();

        this.startPointX = lineXAbsoluteCoordination.left;
        this.startPointY = lineXAbsoluteCoordination.top;
    };

    this.markX = function() {

        var beginLine = this.lineX.offsetLeft;
        var endLine = this.lineX.offsetWidth;

        //positive
        for (var iteration = beginLine; iteration <= endLine; iteration += this.markStep) {

            var mark = document.createElement('div');
            mark.className = 'markX';
            mark.style.left = iteration + 'px';

            this.lineX.appendChild(mark);

            //numbers
            if (iteration != 0) {
                var markNumber = document.createElement('div');
                markNumber.className = 'markNumberX';
                markNumber.style.left = iteration + 'px';

                markNumber.innerHTML = iteration.toString();

                this.lineX.appendChild(markNumber);
            }

        }
    };

    this.markY = function() {
        var beginLine = this.lineY.offsetTop;
        var endLine = this.lineY.offsetHeight;

        var middleLine = this.lineY.offsetHeight/2;

        //positive
        var markNumber = 0;
        for (var iteration = middleLine; iteration >= beginLine; iteration -= this.markStep) {

            var mark = document.createElement('div');
            mark.className = 'markY';
            mark.style.top = iteration + 'px';

            this.lineY.appendChild(mark);

            //numbers
            var markNumberContent = document.createElement('div');
            markNumberContent.className = 'markNumberY';
            markNumberContent.style.top = iteration + 'px';

            markNumberContent.innerHTML = markNumber.toString();

            this.lineY.appendChild(markNumberContent);

            markNumber += this.markStep;


        }

        //negation
        markNumber = 0;
        for (var iteration = middleLine; iteration <= endLine; iteration += this.markStep) {

            var mark = document.createElement('div');
            mark.className = 'markY';
            mark.style.top = iteration + 'px';

            this.lineY.appendChild(mark);

            //numbers
            var markNumberContent = document.createElement('div');
            markNumberContent.className = 'markNumberY';
            markNumberContent.style.top = iteration + 'px';

            markNumberContent.innerHTML = markNumber.toString();

            this.lineY.appendChild(markNumberContent);

            markNumber -= this.markStep;
        }
    };


}