var map = new Map('map');
map.create();

var sinus = new Sinus(map.markStep, map.startPointX, map.startPointY, map.lengthX);
sinus.pulse(0);


var sinController = new SinController(sinus);
sinController.init();