/**
 * Created by ����� on 15.11.2016.
 */
function SinController(sinus) {
    this.sinus = sinus;

    this.controllAmplitude = document.getElementById('amplitude');
    this.controllSpeed = document.getElementById('speed');
    this.controllerFrequency = document.getElementById('frequency');
    this.controllPulseSwitch = document.getElementById('pulseSwitch');
    this.controllTrajectorySwitch = document.getElementById('trajectorySwitch');

    this.init = function() {

        this.controllAmplitude.value = this.sinus.amplitude;
        this.controllSpeed.value = this.sinus.pulseSpeed;
        this.controllerFrequency.value = this.sinus.frequency;

        this.changeAmplitude();
        this.changeSpeed();
        this.changeFrequency();
        this.switchPulse();
        this.switchTrajectory();
    };

    this.changeAmplitude = function() {
        var selfObj = this;
        this.controllAmplitude.onchange = function() {
            selfObj.sinus.amplitude = this.value;
            selfObj.sinus.pulse(0);

            selfObj.resetTrajectory();
        };

    };

    this.changeSpeed = function() {
        var selfObj = this;
        this.controllSpeed.onchange = function() {
            selfObj.sinus.pulseSpeed = this.value;
            selfObj.sinus.pulse(0);
        };
    };

    this.changeFrequency = function() {
        var selfObj = this;
        this.controllerFrequency.onchange = function() {
            selfObj.sinus.frequency = this.value;
            selfObj.sinus.pulse(0);
            selfObj.resetTrajectory();
        };
    };

    this.switchPulse = function() {
        var selfObj = this;
        this.controllPulseSwitch.onclick = function () {
            if (document.getElementsByClassName(selfObj.sinus.pulseClassName)[0] == null) {
                selfObj.sinus.pulse(0);
            } else {
                selfObj.sinus.pulseRemove();
            }
        };
    };

    this.switchTrajectory = function() {
        var selfObj = this;
        this.controllTrajectorySwitch.onclick = function () {
            if (document.getElementsByClassName(selfObj.sinus.pixelClassName)[0] == null) {
                selfObj.sinus.trajectoryOn();
            } else {
                selfObj.sinus.trajectoryOff();
            }
        };
    };


    this.resetTrajectory = function() {
        var selfObj = this;
        if (document.getElementsByClassName(selfObj.sinus.pixelClassName)[0] != null) {
            selfObj.sinus.trajectoryOff();
            selfObj.sinus.trajectoryOn();
        }
    }
}